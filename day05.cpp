#include <stdio.h>
#include <string.h>

#include <iostream>
#include <vector>

// const std::vector<int32_t> numbers_org = { 3,0,4,0,99 };
// const std::vector<int32_t> numbers_org = { 1002,4,3,4,33,99 };
// const std::vector<int32_t> numbers_org = { 3,9,8,9,10,9,4,9,99,-1,8 };
// const std::vector<int32_t> numbers_org = { 3,9,7,9,10,9,4,9,99,-1,8 };
// const std::vector<int32_t> numbers_org = { 3,3,1108,-1,8,3,4,3,99 };
// const std::vector<int32_t> numbers_org = { 3,3,1107,-1,8,3,4,3,99 };
// const std::vector<int32_t> numbers_org = { 3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9 };
// const std::vector<int32_t> numbers_org = { 3,3,1105,-1,9,1101,0,0,12,4,12,99,1 };
// const std::vector<int32_t> numbers_org = { 3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31, 1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104, 999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99 };
const std::vector<int32_t> numbers_org = {
    3,225,1,225,6,6,1100,1,238,225,104,0,1,192,154,224,101,-161,224,224,4,224,102,8,223,223,101,5,224,224,1,223,224,223,1001,157,48,224,1001,224,-61,224,4,224,102,8,223,223,101,2,224,224,1,223,224,223,1102,15,28,225,1002,162,75,224,1001,224,-600,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,102,32,57,224,1001,224,-480,224,4,224,102,8,223,223,101,1,224,224,1,224,223,223,1101,6,23,225,1102,15,70,224,1001,224,-1050,224,4,224,1002,223,8,223,101,5,224,224,1,224,223,223,101,53,196,224,1001,224,-63,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,64,94,225,1102,13,23,225,1101,41,8,225,2,105,187,224,1001,224,-60,224,4,224,1002,223,8,223,101,6,224,224,1,224,223,223,1101,10,23,225,1101,16,67,225,1101,58,10,225,1101,25,34,224,1001,224,-59,224,4,224,1002,223,8,223,1001,224,3,224,1,223,224,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1108,226,226,224,102,2,223,223,1005,224,329,101,1,223,223,107,226,226,224,1002,223,2,223,1005,224,344,1001,223,1,223,107,677,226,224,102,2,223,223,1005,224,359,101,1,223,223,7,677,226,224,102,2,223,223,1005,224,374,101,1,223,223,108,226,226,224,102,2,223,223,1006,224,389,101,1,223,223,1007,677,677,224,102,2,223,223,1005,224,404,101,1,223,223,7,226,677,224,102,2,223,223,1006,224,419,101,1,223,223,1107,226,677,224,1002,223,2,223,1005,224,434,1001,223,1,223,1108,226,677,224,102,2,223,223,1005,224,449,101,1,223,223,108,226,677,224,102,2,223,223,1005,224,464,1001,223,1,223,8,226,677,224,1002,223,2,223,1005,224,479,1001,223,1,223,1007,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,1008,226,677,224,102,2,223,223,1006,224,509,101,1,223,223,1107,677,226,224,1002,223,2,223,1006,224,524,1001,223,1,223,108,677,677,224,1002,223,2,223,1005,224,539,1001,223,1,223,1107,226,226,224,1002,223,2,223,1006,224,554,1001,223,1,223,7,226,226,224,1002,223,2,223,1006,224,569,1001,223,1,223,8,677,226,224,102,2,223,223,1006,224,584,101,1,223,223,1008,677,677,224,102,2,223,223,1005,224,599,101,1,223,223,1007,226,677,224,1002,223,2,223,1006,224,614,1001,223,1,223,8,677,677,224,1002,223,2,223,1005,224,629,101,1,223,223,107,677,677,224,102,2,223,223,1005,224,644,101,1,223,223,1108,677,226,224,102,2,223,223,1005,224,659,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226
};

void parseOpcode(int num, int& opcode, int& first_mode, int& second_mode, int& third_mode) {
    opcode = num%100;
    first_mode=(num/100)%10;
    second_mode=(num/1000)%10;
    third_mode=(num/10000)%10;
}

void calc_val(int32_t input, std::vector<int32_t>& outputs) {
    std::vector<int32_t> numbers(numbers_org.size());
    std::copy(numbers_org.begin(),numbers_org.end(),numbers.begin());


    bool is_done = false;
    for (size_t i = 0; i < numbers.size(); ++i) {
        if (is_done) {
            break;
        }

        int op_code = 99;
        int first_mode = 0;
        int second_mode = 0;
        int third_mode = 0;
        parseOpcode(numbers[i], op_code, first_mode, second_mode, third_mode);

        int32_t addr_1 = numbers[++i];

        switch (op_code) {
            case 1: {
                        int32_t addr_2 = numbers[++i];
                        int32_t addr_res = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];
                        // operation
                        numbers[addr_res] = first_param + second_param;
                    } break;
            case 2: {
                        int32_t addr_2 = numbers[++i];
                        int32_t addr_res = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];
                        // operation
                        numbers[addr_res] = first_param * second_param;
                    } break;
            case 3: {
                        int input_addr = addr_1;
                        numbers[input_addr] = input;
                    } break;
            case 4: {
                        int output_addr = addr_1;
                        if (first_mode) outputs.push_back(output_addr);
                        else outputs.push_back(numbers[output_addr]);
                    } break;
            case 5: {
                        int32_t addr_2 = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];

                        if (first_param) {
                            i = second_param;
                            --i; // keep i the same in the next loop
                        }
                    } break;
            case 6: {
                        int32_t addr_2 = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];

                        if (!first_param) {
                            i = second_param;
                            --i; // keep i the same in the next loop
                        }
                    } break;
            case 7: {
                        int32_t addr_2 = numbers[++i];
                        int32_t addr_res = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];

                        if (first_param < second_param) numbers[addr_res] = 1;
                        else numbers[addr_res] = 0;
                    } break;
            case 8: {
                        int32_t addr_2 = numbers[++i];
                        int32_t addr_res = numbers[++i];
                        // mode stuff
                        int32_t first_param = addr_1;
                        int32_t second_param = addr_2;
                        if (!first_mode) first_param = numbers[addr_1];
                        if (!second_mode) second_param = numbers[addr_2];

                        if (first_param == second_param) numbers[addr_res] = 1;
                        else numbers[addr_res] = 0;
                    } break;
            case 99: {
                         is_done = true;
                     } break;
        }
    }
}

static void part01() {
    std::vector<int32_t> outputs;
    calc_val(1, outputs);
    std::cout << "part 1 outputs:" << std::endl;
    for (auto out : outputs) {
        std::cout << out << std::endl;
    }
}

static void part02() {
    std::vector<int32_t> outputs;
    calc_val(5, outputs);
    std::cout << "part 2 outputs:" << std::endl;
    for (auto out : outputs) {
        std::cout << out << std::endl;
    }
}

int main() {
    part01();
    part02();

    return 0;
}

