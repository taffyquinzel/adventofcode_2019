#include <stdbool.h>
#include <stdio.h>
#include <string.h>

int numbers_org[] = {
    1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,9,19,1,10,19,23,2,9,23,27,1,6,27,31,2,31,9,35,1,5,35,39,1,10,39,43,1,10,43,47,2,13,47,51,1,10,51,55,2,55,10,59,1,9,59,63,2,6,63,67,1,5,67,71,1,71,5,75,1,5,75,79,2,79,13,83,1,83,5,87,2,6,87,91,1,5,91,95,1,95,9,99,1,99,6,103,1,103,13,107,1,107,5,111,2,111,13,115,1,115,6,119,1,6,119,123,2,123,13,127,1,10,127,131,1,131,2,135,1,135,5,0,99,2,14,0,0
};

int calc_val(int a, int b) {
    int numbers[sizeof(numbers_org)];
    memcpy(numbers, numbers_org, sizeof(numbers));

    numbers[1] = a;
    numbers[2] = b;

    bool is_done = false;
    for (int i = 0; i < sizeof(numbers); ++i) {
        if (is_done) {
            break;
        }

        int op_code = numbers[i];
        int addr_1 = numbers[++i];
        int addr_2 = numbers[++i];
        int addr_res = numbers[++i];

        switch (op_code) {
            case 1:
                numbers[addr_res] = numbers[addr_1] + numbers[addr_2];
                break;
            case 2:
                numbers[addr_res] = numbers[addr_1] * numbers[addr_2];
                break;
            case 99:
                is_done = true;
                break;
        }
    }

    return numbers[0];
}

void part01() {
    printf("value at pos 0 is: %d\n", calc_val(12,2));
}

void part02() {
    bool is_found = false;
    for (int i = 0; i < 99; ++i) {
        for (int j = 0; j < 99; ++j) {
            if (calc_val(i, j) == 19690720) {
                printf("for noun: %d and verb: %d it is found.\nanswer: %d", i, j, 100 * i + j);
                return;
            }
        }
    }
}

int main() {
    part01();
    part02();

    return 0;
}

