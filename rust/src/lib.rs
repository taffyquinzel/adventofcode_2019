#[macro_use]
extern crate num_derive;
extern crate num_traits;

mod day07;
mod day09;
mod intcode;

pub fn solve_days() {
    day07::main();
    day09::main();
}
