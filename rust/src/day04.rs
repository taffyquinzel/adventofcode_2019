const START: u32 = 138241;
const END: u32 = 674034;

fn main() {
    let mut nums:Vec<Vec<u32>> = Vec::new();
    let mut nums_res:Vec<Vec<u32>> = Vec::new();
    let mut nums_res2:Vec<Vec<u32>> = Vec::new();

    for number in START..END {
        let mut n = number;
        let mut nrs = Vec::new();
        while n > 0 {
            let rest = n%10;
            n /= 10;
            nrs.push(rest);
        }
        nrs.reverse();
        nums.push(nrs);
    }

    for num in &nums {
        if num.len() != 6 {
            continue;
        }
        let amount_arr = |x, a: &Vec<u32>| a.iter().filter(|&n| *n >= x).count() as u32;
        let amount_arr2 = |x, a: &Vec<u32>| a.iter().filter(|&n| *n == x).count() as u32;
        let amount = |x| amount_arr(x, num);
        let amount2 = |x| amount_arr2(x, num);

        let mut adjacent_duplicates = false;
        let mut adjacent_duplicates2 = vec![amount2(num[0])];
        let mut digits_never_decrease = true;
        for i in 1..num.len() {
            adjacent_duplicates2.push(amount2(num[i]));

            if !adjacent_duplicates && num[i-1] == num[i] {
                adjacent_duplicates = true;
            }
            if digits_never_decrease && num[i-1] <= num[i] {
                digits_never_decrease = true;
            } else {
                digits_never_decrease = false;
            }
        }
        if adjacent_duplicates && digits_never_decrease {
            nums_res.push(num.clone());
        }
        if amount_arr2(2,&adjacent_duplicates2) > 0 && digits_never_decrease {
            nums_res2.push(num.clone());
        }
    }

    println!("{}", nums_res.len());
    println!("{}", nums_res2.len());
}
