use std::fs;

pub fn main() {
    let numbers_org = read_input("../../inputs/09");

    println!("day09:");
    part1();
    part2();
}

fn read_input(file: &str) -> Vec<i32> {
    let mut numbers_org = Vec::new();

    for line in fs::read_to_string(file).unwrap().lines() {
        for num in line.split(',') {
            let int = num.parse::<i32>();
            match int {
                Ok(x) => numbers_org.push(x),
                Err(_) => (),
            }
        }
    }

    numbers_org
}
fn part1() {}

fn part2() {}
