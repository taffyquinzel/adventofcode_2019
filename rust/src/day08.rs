use std::fs::File;
use std::io::prelude::*;

fn main() -> std::io::Result<()> {
    let width = 25;
    let tall = 6;

    let mut file = File::open("inputs/08")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let mut layers = Vec::new();

    let mut layer = Vec::new();
    for c in contents.chars() {
        // collect layers
        if layer.len() == tall * width {
            layers.push(layer);
            layer = Vec::new();
        }
        // collect row
        if c >= '0' && c <= '9' {
            layer.push(c.to_digit(10).unwrap());
        }
    }

    // push the last layer
    if layer.len() > 0 {
        layers.push(layer);
    }

    let mut least_zeros_id = 0;
    let mut least_zeros = 25 * 6 + 1;
    for (i, l) in layers.iter().enumerate() {
        let mut cur_zeros = 0;
        for d in l {
            if *d == 0 {
                cur_zeros += 1;
            }
        }
        if least_zeros > cur_zeros {
            least_zeros = cur_zeros;
            least_zeros_id = i;
        }
    }

    let mut num_1s = 0;
    let mut num_2s = 0;
    for d in layers[least_zeros_id].iter() {
        if *d == 1 {
            num_1s += 1;
        }
        if *d == 2 {
            num_2s += 1;
        }
    }

    println!("Part I: {}", num_1s * num_2s);
    print_image(layers, width, tall);

    Ok(())
}

fn print_image(layers: Vec<Vec<u32>>, width: usize, height: usize) {
    let mut image = vec![2; width * height];

    // calculate pixels
    for i in 0..width * height {
        for l in layers.iter() {
            image[i] = l[i];
            if image[i] != 2 {
                break;
            }
        }
    }

    // build image
    let mut res = String::new();
    for i in 0..height * width {
        if i % width == 0 {
            res.push('\n');
        }
        if image[i] == 1 {
            res.push('X');
        } else {
            res.push(' ');
        }
    }
    println!("Part II:\n{}", res);
}
