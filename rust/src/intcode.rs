use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::ops::{Index, IndexMut};

#[derive(Clone)]
pub struct Numbers<T: Clone> {
    numbers: Vec<T>,
    default_value: T,
}

pub struct Amp {
    name: String,
    numbers: Numbers<i32>,
    i: usize,
    base: i32,
}

#[derive(FromPrimitive)]
enum OperationMode {
    Normal = 0,
    Position = 1,
    Relative = 2,
}

impl<T: Clone + Copy> Numbers<T> {
    pub fn set(&mut self, i: usize, val: T) {
        self.check_size(i);
        self.numbers[i] = val;
    }
    pub fn len(&self) -> usize {
        self.numbers.len()
    }

    pub fn get(&mut self, i: usize) -> T {
        self.check_size(i);
        self.numbers[i]
    }

    pub fn new(nums: Vec<T>, val: T) -> Numbers<T> {
        Numbers {
            numbers: nums,
            default_value: val,
        }
    }

    pub fn empty(val: T) -> Numbers<T> {
        Numbers {
            numbers: Vec::new(),
            default_value: val,
        }
    }

    fn check_size(&mut self, i: usize) {
        match self.numbers.get(i) {
            Some(_) => (),
            None => self.numbers.resize(i + 1, self.default_value),
        };
    }
}

impl Amp {
    pub fn new() -> Amp {
        Amp {
            name: "".to_string(),
            numbers: Numbers::empty(0),
            i: 0,
            base: 0,
        }
    }

    pub fn new_with_numbers(nums: Numbers<i32>, name: &str) -> Amp {
        Amp {
            name: name.to_string(),
            numbers: nums,
            i: 0,
            base: 0,
        }
    }

    pub fn reset(&mut self) {
        self.i = 0;
    }

    pub fn calc_val(&mut self, input: Vec<i32>) -> (bool, i32) {
        let mut outputs = 0;
        let mut input_index = 0;

        let mut is_done = false;
        while self.i < self.numbers.len() {
            if is_done {
                break;
            }

            let (op_code, first_mode, second_mode, _third_mode) =
                parse_opcode(self.numbers.get(self.i));

            match op_code {
                1 => self.op_1(first_mode, second_mode),
                2 => self.op_2(first_mode, second_mode),
                3 => self.op_3(&mut input_index, &input),
                4 => return self.op_4(first_mode, &mut outputs),
                5 => self.op_5(first_mode, second_mode),
                6 => self.op_6(first_mode, second_mode),
                7 => self.op_7(first_mode, second_mode),
                8 => self.op_8(first_mode, second_mode),
                9 => self.op_9(first_mode, second_mode),
                99 => {
                    is_done = true;
                    self.i += 1;
                    return (true, outputs);
                }
                _ => {
                    println!("error parsing opcode: {}", op_code);
                }
            }

            // update iterator
            self.i += 1;
        }

        return (is_done, outputs);
    }

    fn get_next_address(&mut self) -> i32 {
        self.i += 1;
        self.numbers.get(self.i)
    }

    fn get_param(&mut self, mode: OperationMode, addr: i32) -> i32 {
        match mode {
            OperationMode::Normal => self.numbers.get(addr as usize),
            OperationMode::Position => addr,
            OperationMode::Relative => self.base + addr,
        }
    }

    fn op_1(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        let addr_res = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);
        // operation
        self.numbers
            .set(addr_res as usize, first_param + second_param);
    }

    fn op_2(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        let addr_res = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);
        // operation
        self.numbers
            .set(addr_res as usize, first_param * second_param);
    }

    fn op_3(&mut self, input_index: &mut i32, input: &Vec<i32>) {
        let addr_1 = self.get_next_address();
        let input_addr = addr_1;
        self.numbers
            .set(input_addr as usize, input[*input_index as usize]);
        *input_index += 1;
    }

    fn op_4(&mut self, first_mode: OperationMode, outputs: &mut i32) -> (bool, i32) {
        let addr_1 = self.get_next_address();
        let output_addr = addr_1;
        *outputs = self.get_param(first_mode, output_addr);
        self.i += 1; // increment for next instruction

        (false, *outputs)
    }

    fn op_5(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);
        // instruction
        if first_param != 0 {
            self.i = second_param as usize;
            self.i = self.i - 1; // keep self.i the same in the next loop
        }
    }

    fn op_6(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);
        // instruction
        if first_param == 0 {
            self.i = second_param as usize;
            self.i = self.i - 1; // keep self.i the same in the next loop
        }
    }

    fn op_7(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        let addr_res = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);

        self.numbers.set(
            addr_res as usize,
            if first_param < second_param { 1 } else { 0 },
        );
    }

    fn op_8(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        let addr_res = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);

        self.numbers.set(
            addr_res as usize,
            if first_param == second_param { 1 } else { 0 },
        );
    }

    fn op_9(&mut self, first_mode: OperationMode, second_mode: OperationMode) {
        let addr_1 = self.get_next_address();
        let addr_2 = self.get_next_address();
        let addr_res = self.get_next_address();
        // mode stuff
        let first_param = self.get_param(first_mode, addr_1);
        let second_param = self.get_param(second_mode, addr_2);

        self.numbers.set(
            addr_res as usize,
            if first_param == second_param { 1 } else { 0 },
        );
    }
}

fn parse_opcode(num: i32) -> (i32, OperationMode, OperationMode, OperationMode) {
    let opcode = num % 100;
    let first_mode = FromPrimitive::from_i32((num / 100) % 10).unwrap();
    let second_mode = FromPrimitive::from_i32((num / 1000) % 10).unwrap();
    let third_mode = FromPrimitive::from_i32((num / 10000) % 10).unwrap();

    (opcode, first_mode, second_mode, third_mode)
}
