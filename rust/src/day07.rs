use super::intcode::{Amp, Numbers};
use std::fs;

fn read_input(file: &str) -> Vec<i32> {
    let mut numbers_org = Vec::new();

    for line in fs::read_to_string(file).unwrap().lines() {
        for num in line.split(',') {
            let int = num.parse::<i32>();
            match int {
                Ok(x) => numbers_org.push(x),
                Err(_) => (),
            }
        }
    }

    numbers_org
}

fn part01(numbers: Numbers<i32>) {
    let possibilities = vec![
        vec![0, 1, 2, 3, 4],
        vec![0, 1, 2, 4, 3],
        vec![0, 1, 3, 2, 4],
        vec![0, 1, 3, 4, 2],
        vec![0, 1, 4, 3, 2],
        vec![0, 1, 4, 2, 3],
        vec![0, 2, 1, 3, 4],
        vec![0, 2, 1, 4, 3],
        vec![0, 2, 3, 1, 4],
        vec![0, 2, 3, 4, 1],
        vec![0, 2, 4, 3, 1],
        vec![0, 2, 4, 1, 3],
        vec![0, 3, 2, 1, 4],
        vec![0, 3, 2, 4, 1],
        vec![0, 3, 1, 2, 4],
        vec![0, 3, 1, 4, 2],
        vec![0, 3, 4, 1, 2],
        vec![0, 3, 4, 2, 1],
        vec![0, 4, 2, 3, 1],
        vec![0, 4, 2, 1, 3],
        vec![0, 4, 3, 2, 1],
        vec![0, 4, 3, 1, 2],
        vec![0, 4, 1, 3, 2],
        vec![0, 4, 1, 2, 3],
        vec![1, 0, 2, 3, 4],
        vec![1, 0, 2, 4, 3],
        vec![1, 0, 3, 2, 4],
        vec![1, 0, 3, 4, 2],
        vec![1, 0, 4, 3, 2],
        vec![1, 0, 4, 2, 3],
        vec![1, 2, 0, 3, 4],
        vec![1, 2, 0, 4, 3],
        vec![1, 2, 3, 0, 4],
        vec![1, 2, 3, 4, 0],
        vec![1, 2, 4, 3, 0],
        vec![1, 2, 4, 0, 3],
        vec![1, 3, 2, 0, 4],
        vec![1, 3, 2, 4, 0],
        vec![1, 3, 0, 2, 4],
        vec![1, 3, 0, 4, 2],
        vec![1, 3, 4, 0, 2],
        vec![1, 3, 4, 2, 0],
        vec![1, 4, 2, 3, 0],
        vec![1, 4, 2, 0, 3],
        vec![1, 4, 3, 2, 0],
        vec![1, 4, 3, 0, 2],
        vec![1, 4, 0, 3, 2],
        vec![1, 4, 0, 2, 3],
        vec![2, 1, 0, 3, 4],
        vec![2, 1, 0, 4, 3],
        vec![2, 1, 3, 0, 4],
        vec![2, 1, 3, 4, 0],
        vec![2, 1, 4, 3, 0],
        vec![2, 1, 4, 0, 3],
        vec![2, 0, 1, 3, 4],
        vec![2, 0, 1, 4, 3],
        vec![2, 0, 3, 1, 4],
        vec![2, 0, 3, 4, 1],
        vec![2, 0, 4, 3, 1],
        vec![2, 0, 4, 1, 3],
        vec![2, 3, 0, 1, 4],
        vec![2, 3, 0, 4, 1],
        vec![2, 3, 1, 0, 4],
        vec![2, 3, 1, 4, 0],
        vec![2, 3, 4, 1, 0],
        vec![2, 3, 4, 0, 1],
        vec![2, 4, 0, 3, 1],
        vec![2, 4, 0, 1, 3],
        vec![2, 4, 3, 0, 1],
        vec![2, 4, 3, 1, 0],
        vec![2, 4, 1, 3, 0],
        vec![2, 4, 1, 0, 3],
        vec![3, 1, 2, 0, 4],
        vec![3, 1, 2, 4, 0],
        vec![3, 1, 0, 2, 4],
        vec![3, 1, 0, 4, 2],
        vec![3, 1, 4, 0, 2],
        vec![3, 1, 4, 2, 0],
        vec![3, 2, 1, 0, 4],
        vec![3, 2, 1, 4, 0],
        vec![3, 2, 0, 1, 4],
        vec![3, 2, 0, 4, 1],
        vec![3, 2, 4, 0, 1],
        vec![3, 2, 4, 1, 0],
        vec![3, 0, 2, 1, 4],
        vec![3, 0, 2, 4, 1],
        vec![3, 0, 1, 2, 4],
        vec![3, 0, 1, 4, 2],
        vec![3, 0, 4, 1, 2],
        vec![3, 0, 4, 2, 1],
        vec![3, 4, 2, 0, 1],
        vec![3, 4, 2, 1, 0],
        vec![3, 4, 0, 2, 1],
        vec![3, 4, 0, 1, 2],
        vec![3, 4, 1, 0, 2],
        vec![3, 4, 1, 2, 0],
        vec![4, 1, 2, 3, 0],
        vec![4, 1, 2, 0, 3],
        vec![4, 1, 3, 2, 0],
        vec![4, 1, 3, 0, 2],
        vec![4, 1, 0, 3, 2],
        vec![4, 1, 0, 2, 3],
        vec![4, 2, 1, 3, 0],
        vec![4, 2, 1, 0, 3],
        vec![4, 2, 3, 1, 0],
        vec![4, 2, 3, 0, 1],
        vec![4, 2, 0, 3, 1],
        vec![4, 2, 0, 1, 3],
        vec![4, 3, 2, 1, 0],
        vec![4, 3, 2, 0, 1],
        vec![4, 3, 1, 2, 0],
        vec![4, 3, 1, 0, 2],
        vec![4, 3, 0, 1, 2],
        vec![4, 3, 0, 2, 1],
        vec![4, 0, 2, 3, 1],
        vec![4, 0, 2, 1, 3],
        vec![4, 0, 3, 2, 1],
        vec![4, 0, 3, 1, 2],
        vec![4, 0, 1, 3, 2],
        vec![4, 0, 1, 2, 3],
    ];

    let mut a = Amp::new_with_numbers(numbers, "");

    let mut max = 0;
    let mut max_seq = Vec::new();
    for option in possibilities {
        let mut cur_output = 0;
        for phase in option.iter() {
            a.reset();
            let (_done, cur_output_) = a.calc_val(vec![*phase, cur_output]);
            cur_output = cur_output_;
        }
        if max < cur_output {
            max = cur_output;
            max_seq = option;
        }
    }

    // print result
    println!("part 1: {}, {:?}", max, max_seq);
}

fn part02(numbers: Numbers<i32>) {
    let possibilities = vec![
        vec![5, 6, 7, 8, 9],
        vec![5, 6, 7, 9, 8],
        vec![5, 6, 8, 7, 9],
        vec![5, 6, 8, 9, 7],
        vec![5, 6, 9, 8, 7],
        vec![5, 6, 9, 7, 8],
        vec![5, 7, 6, 8, 9],
        vec![5, 7, 6, 9, 8],
        vec![5, 7, 8, 6, 9],
        vec![5, 7, 8, 9, 6],
        vec![5, 7, 9, 8, 6],
        vec![5, 7, 9, 6, 8],
        vec![5, 8, 7, 6, 9],
        vec![5, 8, 7, 9, 6],
        vec![5, 8, 6, 7, 9],
        vec![5, 8, 6, 9, 7],
        vec![5, 8, 9, 6, 7],
        vec![5, 8, 9, 7, 6],
        vec![5, 9, 7, 8, 6],
        vec![5, 9, 7, 6, 8],
        vec![5, 9, 8, 7, 6],
        vec![5, 9, 8, 6, 7],
        vec![5, 9, 6, 8, 7],
        vec![5, 9, 6, 7, 8],
        vec![6, 5, 7, 8, 9],
        vec![6, 5, 7, 9, 8],
        vec![6, 5, 8, 7, 9],
        vec![6, 5, 8, 9, 7],
        vec![6, 5, 9, 8, 7],
        vec![6, 5, 9, 7, 8],
        vec![6, 7, 5, 8, 9],
        vec![6, 7, 5, 9, 8],
        vec![6, 7, 8, 5, 9],
        vec![6, 7, 8, 9, 5],
        vec![6, 7, 9, 8, 5],
        vec![6, 7, 9, 5, 8],
        vec![6, 8, 7, 5, 9],
        vec![6, 8, 7, 9, 5],
        vec![6, 8, 5, 7, 9],
        vec![6, 8, 5, 9, 7],
        vec![6, 8, 9, 5, 7],
        vec![6, 8, 9, 7, 5],
        vec![6, 9, 7, 8, 5],
        vec![6, 9, 7, 5, 8],
        vec![6, 9, 8, 7, 5],
        vec![6, 9, 8, 5, 7],
        vec![6, 9, 5, 8, 7],
        vec![6, 9, 5, 7, 8],
        vec![7, 6, 5, 8, 9],
        vec![7, 6, 5, 9, 8],
        vec![7, 6, 8, 5, 9],
        vec![7, 6, 8, 9, 5],
        vec![7, 6, 9, 8, 5],
        vec![7, 6, 9, 5, 8],
        vec![7, 5, 6, 8, 9],
        vec![7, 5, 6, 9, 8],
        vec![7, 5, 8, 6, 9],
        vec![7, 5, 8, 9, 6],
        vec![7, 5, 9, 8, 6],
        vec![7, 5, 9, 6, 8],
        vec![7, 8, 5, 6, 9],
        vec![7, 8, 5, 9, 6],
        vec![7, 8, 6, 5, 9],
        vec![7, 8, 6, 9, 5],
        vec![7, 8, 9, 6, 5],
        vec![7, 8, 9, 5, 6],
        vec![7, 9, 5, 8, 6],
        vec![7, 9, 5, 6, 8],
        vec![7, 9, 8, 5, 6],
        vec![7, 9, 8, 6, 5],
        vec![7, 9, 6, 8, 5],
        vec![7, 9, 6, 5, 8],
        vec![8, 6, 7, 5, 9],
        vec![8, 6, 7, 9, 5],
        vec![8, 6, 5, 7, 9],
        vec![8, 6, 5, 9, 7],
        vec![8, 6, 9, 5, 7],
        vec![8, 6, 9, 7, 5],
        vec![8, 7, 6, 5, 9],
        vec![8, 7, 6, 9, 5],
        vec![8, 7, 5, 6, 9],
        vec![8, 7, 5, 9, 6],
        vec![8, 7, 9, 5, 6],
        vec![8, 7, 9, 6, 5],
        vec![8, 5, 7, 6, 9],
        vec![8, 5, 7, 9, 6],
        vec![8, 5, 6, 7, 9],
        vec![8, 5, 6, 9, 7],
        vec![8, 5, 9, 6, 7],
        vec![8, 5, 9, 7, 6],
        vec![8, 9, 7, 5, 6],
        vec![8, 9, 7, 6, 5],
        vec![8, 9, 5, 7, 6],
        vec![8, 9, 5, 6, 7],
        vec![8, 9, 6, 5, 7],
        vec![8, 9, 6, 7, 5],
        vec![9, 6, 7, 8, 5],
        vec![9, 6, 7, 5, 8],
        vec![9, 6, 8, 7, 5],
        vec![9, 6, 8, 5, 7],
        vec![9, 6, 5, 8, 7],
        vec![9, 6, 5, 7, 8],
        vec![9, 7, 6, 8, 5],
        vec![9, 7, 6, 5, 8],
        vec![9, 7, 8, 6, 5],
        vec![9, 7, 8, 5, 6],
        vec![9, 7, 5, 8, 6],
        vec![9, 7, 5, 6, 8],
        vec![9, 8, 7, 6, 5],
        vec![9, 8, 7, 5, 6],
        vec![9, 8, 6, 7, 5],
        vec![9, 8, 6, 5, 7],
        vec![9, 8, 5, 6, 7],
        vec![9, 8, 5, 7, 6],
        vec![9, 5, 7, 8, 6],
        vec![9, 5, 7, 6, 8],
        vec![9, 5, 8, 7, 6],
        vec![9, 5, 8, 6, 7],
        vec![9, 5, 6, 8, 7],
        vec![9, 5, 6, 7, 8],
    ];

    let mut max = 0;
    let mut max_seq = Vec::new();
    for option in possibilities {
        let mut amps = vec![
            Amp::new_with_numbers(numbers.clone(), "A"),
            Amp::new_with_numbers(numbers.clone(), "B"),
            Amp::new_with_numbers(numbers.clone(), "C"),
            Amp::new_with_numbers(numbers.clone(), "D"),
            Amp::new_with_numbers(numbers.clone(), "E"),
        ];
        let mut cur_output = 0;
        let mut first_loop = true;
        'first: loop {
            for i in 0..5 {
                let (done, cur_output_) = amps[i].calc_val(if first_loop {
                    vec![option[i], cur_output]
                } else {
                    vec![cur_output]
                });
                if done {
                    break 'first;
                } else {
                    cur_output = cur_output_;
                }
            }
            first_loop = false;
        }
        if max < cur_output {
            max = cur_output;
            max_seq = option.clone();
        }
    }
    // print result
    println!("part 2: {}, {:?}", max, max_seq);
}

pub fn main() {
    let numbers_org = Numbers::new(read_input("../../inputs/07"), 0);

    part01(numbers_org.clone());
    part02(numbers_org.clone());
}
